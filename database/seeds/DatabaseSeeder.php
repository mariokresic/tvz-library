<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call(BookCategoriesTableSeeder::class);
        $this->call(RolesTableSeeder::class);

        $this->call(AuthorsTableSeeder::class);
        $this->call(BookTitlesTableSeeder::class);
        $this->call(BooksTableSeeder::class);

        $this->call(UsersTableSeeder::class);

        //$this->call(BookRentalsTableSeeder::class);
    }
}
