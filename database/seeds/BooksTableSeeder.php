<?php

use App\Book;
use App\BookTitle;
use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bookTitles = BookTitle::all();

        foreach ($bookTitles as $bookTitle) {
            $booksCount = rand(1, 5);

            for ($i = 0; $i < $booksCount; $i++) {
                factory(Book::class)->create([
                    'book_title_id' => $bookTitle->id
                ]);
            }
        }
    }
}
