<?php

use App\BookRental;
use Illuminate\Database\Seeder;

class BookRentalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(BookRental::class, 100);
    }
}
