<?php

use App\Author;
use App\BookTitle;
use Illuminate\Database\Seeder;

class BookTitlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authors = Author::all();

        if ($authors->count() < 50)
            $authors = factory(Author::class, 50)->create();

        foreach ($authors as $author) {
            $bookTitlesCount = rand(1, 10);

            for ($i = 0; $i < $bookTitlesCount; $i++) {
                factory(BookTitle::class)->create([
                    'author_id' => $author->id
                ]);
            }
        }
    }
}
