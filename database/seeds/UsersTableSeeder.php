<?php

use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // admin acc
        User::create([
            'name' => 'Admin Adminic',
            'email' => 'admin@email.com',
            'email_verified_at' => now(),
            'password' => Hash::make('102030'), // 102030
            'remember_token' => Str::random(10),
            'role_id' => Role::where('name', 'admin')->first()->id
        ]);

        // employee acc
        User::create([
            'name' => 'Zaposlenik Zaposlic',
            'email' => 'employee@email.com',
            'email_verified_at' => now(),
            'password' => Hash::make('102030'), // 102030
            'remember_token' => Str::random(10),
            'role_id' => Role::where('name', 'employee')->first()->id
        ]);

        // 50 normal users
        // todo: create algorithm for filling out subscriptions table
        factory(User::class, 50)->create();
    }
}
