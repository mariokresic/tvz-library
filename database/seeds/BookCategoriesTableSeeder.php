<?php

use App\BookCategory;
use Illuminate\Database\Seeder;

class BookCategoriesTableSeeder extends Seeder
{
    const CATEGORIES = [
        'Art & Music',
        'Biographies',
        'Business',
        'Kids',
        'Comics',
        'Computers & Tech',
        'Cooking',
        'Hobbies & Crafts',
        'Edu & Reference',
        'Health & Fitness',
        'Home & Garden',
        'Horror',
        'Entertainment',
        'Literature & Fiction',
        'Medical',
        'Mysteries',
        'Parenting',
        'Social Sciences',
        'Religion',
        'Romance',
        'Science & Math',
        'Sci-Fi & Fantasy',
        'Self-Help',
        'Sports',
        'Teen',
        'Travel',
        'True Crime',
        'Westerns'
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(self::CATEGORIES as $category) {
            BookCategory::create(['name' => $category]);
        }
    }
}
