<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use App\BookTitle;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'barcode' => Str::random(10),
        'book_title_id' => BookTitle::inRandomOrder()->first()->id,
        'publication' => $faker->numberBetween(1, 10),
        'publication_year' => $faker->year,
        'is_available' => true
    ];
});
