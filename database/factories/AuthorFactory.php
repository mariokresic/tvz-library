<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Author;
use Faker\Generator as Faker;

$factory->define(Author::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'birth_year' => $faker->year(1995),
        'birth_country' => $faker->country,
        'bio' => $faker->text()
    ];
});
