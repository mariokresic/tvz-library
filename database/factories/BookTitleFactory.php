<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Author;
use App\BookCategory;
use App\BookTitle;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(BookTitle::class, function (Faker $faker) {
    return [
        'title' => $faker->text(30),
        'isbn' => Str::random(13),
        'book_category_id' => BookCategory::inRandomOrder()->first()->id,
        'author_id' => Author::inRandomOrder()->first()->id
    ];
});
