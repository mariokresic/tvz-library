<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('barcode');
            $table->unsignedBigInteger('book_title_id');
            $table->string('publication');
            $table->year('publication_year');
            $table->boolean('is_available');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('book_title_id')->references('id')->on('book_titles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
