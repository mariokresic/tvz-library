<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_titles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('isbn');
            $table->string('file')->nullable();
            $table->unsignedBigInteger('book_category_id');
            $table->unsignedBigInteger('author_id');
            $table->timestamps();

            $table->foreign('book_category_id')->references('id')->on('book_categories');
            $table->foreign('author_id')->references('id')->on('authors');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_titles');
    }
}
