@extends('layouts.app')

@section('content')
    <div class="container mt-4">

        <div class="mb-3">
            <a href="{{ route('book-titles.show', $book->book_title_id) }}" class="mb-5">
                <i class="fas fa-arrow-left"></i>
                {{ $book->bookTitle->title }}
            </a>
        </div>

        <div class="row justify-content-end">
            @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
                <a href="{{ route('book-rentals.rent-book', $book->id) }}" class="btn btn-primary col-sm-2 mr-3">
                    Rent
                    <i class="ml-2 fas fa-edit"></i>
                </a>
                <a href="{{ route('books.edit', $book->id) }}" class="btn btn-primary col-sm-2">
                    Edit
                    <i class="ml-2 fas fa-edit"></i>
                </a>
            @endif
            @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
                <form action="{{ route('books.destroy', $book->id) }}" METHOD="post" class="col-sm-2 deleteForm">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger btn-block ">
                        Delete
                        <i class="ml-2 fas fa-trash-alt"></i>
                    </button>
                </form>
            @endif
        </div>

        <h1 class="display-3">
            <i class="fas fa-book"></i>
            <a href="{{ route('book-titles.show', $book->book_title_id) }}">
                {{ $book->bookTitle->title }}
            </a>
        </h1>

        <div>Barcode</div>
        <h5 class="font-weight-bold">
            {{ $book->barcode }}
        </h5>
        <div>Publication</div>
        <h5 class="font-weight-bold">
            {{ $book->publication }}
        </h5>
        <div>Publication Year</div>
        <h5 class="font-weight-bold">
            {{ $book->publication_year }}
        </h5>
        <div>Available</div>
        <h5 class="font-weight-bold">
            @if($book->is_available)
                <i class="fas fa-check text-success"></i>
            @else
                <i class="fas fa-times text-danger"></i>
            @endif
        </h5>

        @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
        <hr>

        <h4 class="mt-5">
            <i class="fas fa-book-open"></i>
            Rents
        </h4>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th>User</th>
                <th>Rented at</th>
                <th>Returned</th>
            </tr>
            </thead>
            <tbody>
            @foreach($book->bookRentals as $rental)
                <tr>
                    <td>
                        {{ $rental->id }}
                    </td>
                    <td>
                        <a href="{{ route('users.show', $rental->user_id) }}">
                            {{ $rental->user->name }}
                        </a>
                    </td>
                    <td>{{ $rental->created_at->format('d.m.Y.') }}</td>
                    <td>
                        @if($rental->is_returned)
                            <i class="fas fa-check text-success"></i>
                            {{ $rental->returned_at->format('d.m.Y.') }}
                        @else
                            <i class="fas fa-times text-danger"></i>
                            <form action="{{ route('book-rentals.return', $rental->book_id) }}" method="post" class="d-inline">
                                @csrf
                                <button type="submit" class="btn btn-secondary btn-sm">
                                    Return
                                </button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @endif
    </div>
@endsection
