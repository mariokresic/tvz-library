@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="mb-3">
            <a href="{{ $book->id ? route('books.show', $book->id) : route('book-titles.show', $bookTitle->id) }}" class="mb-5">
                <i class="fas fa-arrow-left"></i>
                Back
            </a>
        </div>


        <h1 class="display-3">
            <i class="fas fa-book"></i>
            {{ $book->id ? 'Edit' : 'Create' }} Book
        </h1>

        <form method="POST" action="{{ $book->id ? route('books.update', $book->id) : route('books.store') }}">
            @csrf
            @if($book->id)
                @method('PUT')
            @endif
            <input type="hidden" name="book_title_id" value="{{ $book->id ? $book->book_title_id : $bookTitle->id }}" />

            <div class="form-group">
                <label for="barcode">Barcode</label>
                <input class="form-control @error('barcode') is-invalid @enderror" type="text" name="barcode"
                       value="{{old('barcode', $book->barcode)}}"
                       placeholder="Enter barcode"
                />
                @error('barcode')
                <div class="invalid-feedback">{{ $errors->first('barcode') }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="publication">Publication</label>
                <input class="form-control @error('publication') is-invalid @enderror" type="text" name="publication"
                       value="{{old('publication', $book->publication)}}"
                       placeholder="Enter Publication"
                />
                @error('publication')
                <div class="invalid-feedback">{{ $errors->first('publication') }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="publication_year">Publication Year</label>
                <input class="form-control @error('publication_year') is-invalid @enderror" type="text" name="publication_year"
                       value="{{old('publication_year', $book->publication_year)}}"
                       placeholder="Enter Publication Year"
                />
                @error('publication_year')
                <div class="invalid-feedback">{{ $errors->first('publication_year') }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
