<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TVZ Library') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://kit.fontawesome.com/2600d9782b.js" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app" class="full-height">
    @include('partials._navbar')

    <main>
        <!--<editor-fold desc="toast">!-->
        @if(Session::has('success-message'))
            <div class="toast">
                <div class="toast-header bg-primary text-white">
                    <strong class="mr-auto">Info</strong>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    {{ Session::get('success-message') }}
                </div>
            </div>
        @elseif(Session::has('danger-message'))
            <div class="toast">
                <div class="toast-header bg-danger text-white">
                    <strong class="mr-auto">Info</strong>
                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="toast-body">
                    {{ Session::get('danger-message') }}
                </div>
            </div>
        @endif
        <!--</editor-fold>!-->

        @yield('content')
    </main>
</div>
</body>
</html>

<script>
  $('document').ready(() => {
    jQuery(document).ready(function($){
      $('.deleteForm').on('submit',function(e){
        if(!confirm('Do you want to delete this item?')){
          e.preventDefault();
        }
      });
    });

    @if(Session::has('success-message') || Session::has('danger-message'))
      let options = { delay: 10*1000 }
      let toastr = $('.toast').toast(options)
      toastr.toast('show')
    @endif
  })
</script>
