@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="mb-3">
            <a href="{{ $user->id ? route('users.show', $user->id) : route('users.index') }}" class="mb-5">
                <i class="fas fa-arrow-left"></i>
                Back
            </a>
        </div>


        <h1 class="display-3">
            <i class="fas fa-user"></i>
            {{ $user->id ? 'Edit' : 'Create' }} User
        </h1>

        <form method="POST" action="{{ $user->id ? route('users.update', $user->id) : route('users.store') }}">
            @csrf
            @if($user->id)
                @method('PUT')
            @endif
            @if(auth()->user()->isAdmin())
                <div class="form-group">
                    <label for="role_id">Role</label>
                    <select class="form-control @error('role_id') is-invalid @enderror" name="role_id">
                        <option value="">Select role</option>
                        @foreach($roles as $role)
                            <option value="{{ $role->id }}"
                                    @if($role->id == old('role_id', $user->role_id)) selected @endif
                            >
                                {{ ucfirst($role->name) }}
                            </option>
                        @endforeach
                    </select>
                    @error('role_id')
                    <div class="invalid-feedback">{{ $errors->first('role_id') }}</div>
                    @enderror
                </div>
            @else
                <input type="hidden" name="role_id" value="{{ $roles->where('name', 'user')->first()->id }}" />
            @endif
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control @error('name') is-invalid @enderror" type="text" name="name"
                       value="{{old('name', $user->name)}}"
                       placeholder="Enter name"
                />
                @error('name')
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input class="form-control @error('email') is-invalid @enderror" type="email" name="email"
                       value="{{old('email', $user->email)}}"
                       placeholder="Enter email"
                />
                @error('email')
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
