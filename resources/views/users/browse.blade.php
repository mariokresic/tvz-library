@extends('layouts.app')

@section('content')
    <div class="container mt-4">

        <div class="row justify-content-end">
            <a href="{{ route('users.create') }}" class="btn btn-primary col-sm-2">Add New</a>
        </div>

        <h1 class="display-3 mb-4">
            <i class="fas fa-users"></i>
            Users
        </h1>

        <form class="py-3 px-3 row justify-content-between" action="{{ route('users.index') }}" method="GET">
            <div class="form-group">
                <label for="title">Name</label>
                <input class="form-control" type="text" name="name"
                       value="{{ request()->query('name') }}"
                       placeholder="Filter by name"
                />
            </div>
            <div class="form-group">
                <label for="title">Email</label>
                <input class="form-control" type="text" name="email"
                       value="{{ request()->query('email') }}"
                       placeholder="Filter by email"
                />
            </div>
            <div class="col-sm-2 flex-center">
                <button type="submit" class="btn btn-primary btn-block">
                    Filter
                    <i class="ml-2 fas fa-search"></i>
                </button>
            </div>
        </form>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Rented Books</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>
                        <a href="{{ route('users.show', $user->id) }}">
                            {{ $user->name }}
                        </a>
                    </td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->bookRentals->count() > 0 ? $user->bookRentals->count() : 'n/a' }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
    </div>
@endsection
