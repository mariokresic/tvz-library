@extends('layouts.app')

@section('content')
    <div class="container mt-4">

        <div class="mb-3">
            <a href="{{ route('users.index') }}" class="mb-5">
                <i class="fas fa-arrow-left"></i>
                All Users
            </a>
        </div>

        <div class="row justify-content-end">
            <a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary col-sm-2">
                Edit
                <i class="ml-2 fas fa-edit"></i>
            </a>
            <form action="{{ route('users.destroy', $user->id) }}" METHOD="post" class="col-sm-2 deleteForm">
                @method('delete')
                @csrf
                <button type="submit" class="btn btn-danger btn-block ">
                    Delete
                    <i class="ml-2 fas fa-trash-alt"></i>
                </button>
            </form>
        </div>

        <h1 class="display-3">
            <i class="fas fa-user"></i>
            {{ $user->name  }}
        </h1>

        <div>Email</div>
        <h5 class="font-weight-bold">{{ $user->email }}</h5>


        <hr />

        <h4 class="mt-5">Rented Books</h4>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th>Book Title</th>
                <th>Book</th>
                <th>Rented At</th>
                <th>Returned At</th>
            </tr>
            </thead>
            <tbody>
            @foreach($user->bookRentals as $bookRental)
                <tr>
                    <td scope="row">{{ $bookRental->id }}</td>
                    <td>
                        <a href="{{ route('book-titles.show', $bookRental->book->book_title_id) }}">
                            {{ $bookRental->book->bookTitle->title }}
                        </a>
                    </td>
                    <td>
                        <a href="{{ route('books.show', $bookRental->book_id) }}">
                            {{ $bookRental->book->barcode }}
                        </a>
                    </td>
                    <td>{{ $bookRental->book->created_at->format('d.m.Y.') }}</td>
                    <td>
                        @if($bookRental->is_returned)
                            {{ $bookRental->returned_at->format('d.m.Y.') }}
                        @else
                            <i class="fas fa-times text-danger"></i>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
