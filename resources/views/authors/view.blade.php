@extends('layouts.app')

@section('content')
    <div class="container mt-4">

        <div class="mb-3">
            <a href="{{ route('authors.index') }}" class="mb-5">
                <i class="fas fa-arrow-left"></i>
                All Authors
            </a>
        </div>

        <div class="row justify-content-end">
            @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
                <a href="{{ route('authors.edit', $author->id) }}" class="btn btn-primary col-sm-2">Edit</a>
            @endif
            @if(auth()->user()->isAdmin())
                <form action="{{ route('authors.destroy', $author->id) }}" METHOD="post" class="col-sm-2 deleteForm">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger btn-block ">
                        Delete
                        <i class="ml-2 fas fa-trash-alt"></i>
                    </button>
                </form>
            @endif
        </div>

        <h1 class="display-3">
            <i class="fas fa-at"></i>
            {{ $author->name  }}
        </h1>

        <div>Birth year</div>
        <h5 class="font-weight-bold">{{ $author->birth_year }}</h5>
        <div>Birth country</div>
        <h5 class="font-weight-bold">{{ $author->birth_country }}</h5>
        <div>Bio</div>
        <h5 class="font-weight-bold">{{ $author->bio }}</h5>

        <hr />

        <h4 class="mt-5">Book Titles</h4>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th>Title</th>
                <th>Category</th>
            </tr>
            </thead>
            <tbody>
            @foreach($author->bookTitles as $bookTitle)
                <tr>
                    <th scope="row">{{ $bookTitle->id }}</th>
                    <th>
                        <a href="{{ route('book-titles.show', $bookTitle->id) }}">
                            {{ $bookTitle->title }}
                        </a>
                    </th>
                    <th>{{ $bookTitle->bookCategory->name }}</th>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
