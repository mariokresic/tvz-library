@extends('layouts.app')

@section('content')
   <div class="container mt-4">
       @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
           <div class="row justify-content-end">
               <a href="{{ route('authors.create') }}" class="btn btn-primary col-sm-2">Add New</a>
           </div>
       @endif
       <h1 class="display-3 mb-4">
           <i class="fas fa-at"></i>
           Authors
       </h1>

       <form class="py-3 px-3 row justify-content-between" action="{{ route('authors.index') }}" method="GET">
           <div class="form-group">
               <label for="name">Name</label>
               <input class="form-control" type="text" name="name"
                      value="{{ request()->query('name') }}"
                      placeholder="Filter by name"
               />
           </div>
           <div class="form-group">
               <label for="birth_country">Birth Country</label>
               <input class="form-control" type="text" name="birth_country"
                      value="{{ request()->query('birth_country') }}"
                      placeholder="Filter by birth country"
               />
           </div>
           <div class="col-sm-2 flex-center">
               <button type="submit" class="btn btn-primary btn-block">
                   Filter
                   <i class="ml-2 fas fa-search"></i>
               </button>
           </div>
       </form>

       <table class="table">
           <thead>
           <tr>
               <th scope="col">#</th>
               <th scope="col">Name</th>
               <th scope="col">Birth Year</th>
               <th scope="col">Birth Country</th>
               <th scope="col">Book titles</th>
           </tr>
           </thead>
           <tbody>
           @foreach($authors as $author)
               <tr>
                   <td>{{ $author->id }}</td>
                   <td>
                       <a href="{{ route('authors.show', $author->id) }}">
                           {{ $author->name }}
                       </a>
                   </td>
                   <td>{{ $author->birth_year }}</td>
                   <td>{{ $author->birth_country }}</td>
                   <td>{{ $author->bookTitles->count() }}</td>
               </tr>
           @endforeach
           </tbody>
       </table>
       {{ $authors->links() }}
   </div>
@endsection
