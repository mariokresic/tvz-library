@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="mb-3">
            <a href="{{ $author->id ? route('authors.show', $author->id) : route('authors.index') }}" class="mb-5">
                <i class="fas fa-arrow-left"></i>
                Back
            </a>
        </div>


        <h1 class="display-3">
            <i class="fas fa-at"></i>
            {{ $author->id ? 'Edit' : 'Create' }} Author
        </h1>

        <form method="POST" action="{{ $author->id ? route('authors.update', $author->id) : route('authors.store') }}">
            @csrf
            @if($author->id)
                @method('PUT')
            @endif
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control @error('name') is-invalid @enderror" type="text" name="name"
                       value="{{old('name', $author->name)}}"
                       placeholder="Enter name"
                />
                @error('name')
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="birth_year">Birth Year</label>
                <input class="form-control @error('birth_year') is-invalid @enderror" type="number" name="birth_year"
                       min="1950" max="{{ now()->year - 15 }}"
                       value="{{old('birth_year', $author->birth_year)}}"
                       placeholder="Enter birth year"
                />
                @error('birth_year')
                    <div class="invalid-feedback">{{ $errors->first('birth_year') }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="birth_country">Birth Country</label>
                <input class="form-control @error('birth_country') is-invalid @enderror" type="text" name="birth_country"
                       value="{{old('birth_country', $author->birth_country)}}"
                       placeholder="Enter birth country"
                />
                @error('birth_country')
                    <div class="invalid-feedback">{{ $errors->first('birth_country') }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea class="form-control @error('bio') is-invalid @enderror"
                      name="bio" placeholder="Enter bio"
                >{{old('bio', $author->bio)}}</textarea>

                @error('bio')
                    <div class="invalid-feedback">{{ $errors->first('bio') }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
