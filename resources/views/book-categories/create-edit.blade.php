@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="mb-3">
            <a href="{{ $bookCategory->id ? route('book-categories.show', $bookCategory->id) : route('book-categories.index') }}" class="mb-5">
                <i class="fas fa-arrow-left"></i>
                Back
            </a>
        </div>


        <h1 class="display-3">
            <i class="fas fa-th-list"></i>
            {{ $bookCategory->id ? 'Edit' : 'Create' }} Book Category
        </h1>

        <form method="POST" action="{{ $bookCategory->id ? route('book-categories.update', $bookCategory->id) : route('book-categories.store') }}">
            @csrf
            @if($bookCategory->id)
                @method('PUT')
            @endif
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control @error('name') is-invalid @enderror" type="text" name="name"
                       value="{{old('name', $bookCategory->name)}}"
                       placeholder="Enter name"
                />
                @error('name')
                    <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
