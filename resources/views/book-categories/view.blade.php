@extends('layouts.app')

@section('content')
    <div class="container mt-4">

        <div class="mb-3">
            <a href="{{ route('book-categories.index') }}" class="mb-5">
                <i class="fas fa-arrow-left"></i>
                All Book Categories
            </a>
        </div>

        <div class="row justify-content-end">
            @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
            <a href="{{ route('book-categories.edit', $bookCategory->id) }}" class="btn btn-primary col-sm-2">
                Edit
                <i class="ml-2 fas fa-edit"></i>
            </a>
            @endif
            @if(auth()->user()->isAdmin())
                <form action="{{ route('book-categories.destroy', $bookCategory->id) }}" METHOD="post" class="col-sm-2 deleteForm">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger btn-block ">
                        Delete
                        <i class="ml-2 fas fa-trash-alt"></i>
                    </button>
                </form>
            @endif
        </div>

        <h1 class="display-3">
            <i class="fas fa-th-list"></i>
            {{ $bookCategory->name  }}
        </h1>
    </div>
@endsection
