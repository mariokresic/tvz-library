@extends('layouts.app')

@section('content')
    <div class="container mt-4">

        @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
        <div class="row justify-content-end">
            <a href="{{ route('book-categories.create') }}" class="btn btn-primary col-sm-2">Add New</a>
        </div>
        @endif

        <h1 class="display-3 mb-4">
            <i class="fas fa-th-list"></i>
            Book Categories
        </h1>

        <form class="py-3 px-3 row justify-content-between" action="{{ route('book-categories.index') }}" method="GET">
            <div class="form-group">
                <label for="title">Name</label>
                <input class="form-control" type="text" name="name"
                       value="{{ request()->query('name') }}"
                       placeholder="Filter by name"
                />
            </div>
            <div class="col-sm-2 flex-center">
                <button type="submit" class="btn btn-primary btn-block">
                    Filter
                    <i class="ml-2 fas fa-search"></i>
                </button>
            </div>
        </form>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
            </tr>
            </thead>
            <tbody>
            @foreach($bookCategories as $bookCategory)
                <tr>
                    <th scope="row">{{ $bookCategory->id }}</th>
                    <td>
                        <a href="{{ route('book-categories.show', $bookCategory->id) }}">
                            {{ $bookCategory->name }}
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $bookCategories->links() }}
    </div>
@endsection
