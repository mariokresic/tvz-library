@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="mb-3">
            <a href="{{ url()->previous() }}" class="mb-5">
                <i class="fas fa-arrow-left"></i>
                Back
            </a>
        </div>


        <h1 class="display-3">
            <i class="fas fa-book"></i>
            Rent {{ $book->bookTitle->title }}
        </h1>

        <form method="POST" action="{{ route('book-rentals.rent', $book->id) }}">
            @csrf
            <div class="form-group">
                <label for="user_id">User</label>
                <select class="form-control @error('user_id') is-invalid @enderror" name="user_id">
                    <option value="">Select user</option>
                    @foreach($users as $user)
                        <option value="{{ $user->id }}">
                            {{ $user->name }}
                        </option>
                    @endforeach
                </select>
                @error('book_category_id')
                <div class="invalid-feedback">{{ $errors->first('book_category_id') }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
