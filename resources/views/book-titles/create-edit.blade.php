@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <div class="mb-3">
            <a href="{{ $bookTitle->id ? route('book-titles.show', $bookTitle->id) : route('book-titles.index') }}" class="mb-5">
                <i class="fas fa-arrow-left"></i>
                Back
            </a>
        </div>


        <h1 class="display-3">
            <i class="fas fa-book"></i>
            {{ $bookTitle->id ? 'Edit' : 'Create' }} Book Title
        </h1>

        <form method="POST" action="{{ $bookTitle->id ? route('book-titles.update', $bookTitle->id) : route('book-titles.store') }}">
            @csrf
            @if($bookTitle->id)
                @method('PUT')
            @endif
            <div class="form-group">
                <label for="title">Title</label>
                <input class="form-control @error('title') is-invalid @enderror" type="text" name="title"
                       value="{{old('title', $bookTitle->title)}}"
                       placeholder="Enter title"
                />
                @error('title')
                <div class="invalid-feedback">{{ $errors->first('title') }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="isbn">ISBN</label>
                <input class="form-control @error('isbn') is-invalid @enderror" type="text" name="isbn"
                       value="{{old('isbn', $bookTitle->isbn)}}"
                       placeholder="Enter ISBN"
                />
                @error('isbn')
                <div class="invalid-feedback">{{ $errors->first('isbn') }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="book_category_id">Category</label>
                <select class="form-control @error('book_category_id') is-invalid @enderror" name="book_category_id">
                    <option value="">Select category</option>
                    @foreach($bookCategories as $category)
                        <option value="{{ $category->id }}"
                                @if($category->id == old('book_category_id', $bookTitle->book_category_id)) selected @endif
                        >
                            {{ $category->name }}
                        </option>
                    @endforeach
                </select>
                @error('book_category_id')
                <div class="invalid-feedback">{{ $errors->first('book_category_id') }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="author_id">Author</label>
                <select class="form-control @error('author_id') is-invalid @enderror" name="author_id">
                    <option value="">Select author</option>
                    @foreach($authors as $author)
                        <option value="{{ $author->id }}"
                                @if($author->id == old('author_id', $bookTitle->author_id)) selected @endif
                        >
                            {{ $author->name }}
                        </option>
                    @endforeach
                </select>
                @error('author_id')
                <div class="invalid-feedback">{{ $errors->first('author_id') }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
