@extends('layouts.app')

@section('content')
    <div class="container mt-4">

        <div class="mb-3">
            <a href="{{ route('book-titles.index') }}" class="mb-5">
                <i class="fas fa-arrow-left"></i>
                All Book Titles
            </a>
        </div>

        <div class="row justify-content-end">
            @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
            <a href="{{ route('book-titles.edit', $bookTitle->id) }}" class="btn btn-primary col-sm-2">
                Edit
                <i class="ml-2 fas fa-edit"></i>
            </a>
            @endif
            @if(auth()->user()->isAdmin())
                <form action="{{ route('book-titles.destroy', $bookTitle->id) }}" METHOD="post" class="col-sm-2 deleteForm">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger btn-block ">
                        Delete
                        <i class="ml-2 fas fa-trash-alt"></i>
                    </button>
                </form>
            @endif
        </div>

        <div class="row  mt-4">
            <div class="flex-shrink-0">
                <img src="{{ asset('storage/'.$bookTitle->file) }}" alt="Book Cover" height="400px">
            </div>
            <div class="flex-grow-1 pl-4">
                <h1 class="display-3">
                    <i class="fas fa-book"></i>
                    {{ $bookTitle->title  }}
                </h1>
                <div>Author</div>
                <h5 class="font-weight-bold">
                    <a href="{{ route('authors.show', $bookTitle->author_id) }}">
                        {{ $bookTitle->author->name }}
                    </a>
                </h5>
                <div>Book Category</div>
                <h5 class="font-weight-bold">
                    <a href="{{ route('book-categories.show', $bookTitle->book_category_id) }}">
                        {{ $bookTitle->bookCategory->name }}
                    </a>
                </h5>
            </div>
        </div>

        <hr />

        <h4 class="mt-5">
            <i class="fas fa-book-open"></i>
            Books
        </h4>
        @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
        <div class="row justify-content-end mb-2">
            <a href="{{ route('books.create', $bookTitle->id) }}" class="btn btn-primary col-sm-2">Add New</a>
        </div>
        @endif

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th>Publication</th>
                <th>Year</th>
                <th>Available</th>
                @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
                <th></th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($bookTitle->books as $book)
                <tr>
                    <td>
                        <a href="{{ route('books.show', $book->id) }}">
                            {{ $book->barcode }}
                        </a>
                    </td>
                    <td>{{ $book->publication }}</td>
                    <td>{{ $book->publication_year }}</td>
                    <td>
                        @if($book->is_available)
                            <i class="fas fa-check text-success"></i>
                        @else
                            <i class="fas fa-times text-danger"></i>
                        @endif
                    </td>
                    <td>
                        <div class="form-inline">
                            @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
                            <a href="{{ route('books.edit', $book->id) }}" class="btn btn-primary">
                                <i class="fas fa-edit"></i>
                            </a>
                            @endif
                            @if(auth()->user()->isAdmin())
                            <form action="{{ route('books.destroy', $book->id) }}" METHOD="post" class="deleteForm">
                                @method('delete')
                                @csrf
                                <button type="submit" class="ml-2 btn btn-danger">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </form>
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
