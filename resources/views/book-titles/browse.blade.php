@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
        <div class="row justify-content-end">
            <a href="{{ route('book-titles.create') }}" class="btn btn-primary col-sm-2">Add New</a>
        </div>
        @endif

        <h1 class="display-3 mb-4">
            <i class="fas fa-book"></i>
            Book Titles
        </h1>

        <form class="py-3 px-3 row justify-content-between" action="{{ route('book-titles.index') }}" method="GET">
            <div class="form-group">
                <label for="title">Title</label>
                <input class="form-control" type="text" name="title"
                       value="{{ request()->query('title') }}"
                       placeholder="Filter by title"
                />
            </div>
            <div class="form-group">
                <label for="author_id">Author</label>
                <select class="form-control" name="author_id">
                    <option value="">Filter by author</option>
                    @foreach($authors as $author)
                        <option value="{{ $author->id }}"
                                @if($author->id == request()->query('author_id')) selected @endif
                        >
                            {{ $author->name }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="book_category_id">Book Category</label>
                <select class="form-control" name="book_category_id">
                    <option value="">Filter by category</option>
                    @foreach($bookCategories as $category)
                        <option value="{{ $category->id }}"
                                @if($category->id == request()->query('book_category_id')) selected @endif
                        >
                            {{ $category->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="col-sm-2 flex-center">
                <button type="submit" class="btn btn-primary btn-block">
                    Filter
                    <i class="ml-2 fas fa-search"></i>
                </button>
            </div>
        </form>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col"></th>
                <th scope="col">Title</th>
                <th scope="col">Category</th>
                <th scope="col">Author</th>
                <th scope="col">Available</th>
            </tr>
            </thead>
            <tbody>
            @foreach($bookTitles as $bookTitle)
                <tr>
                    <td scope="row">{{ $bookTitle->id }}</td>
                    <td><img src="{{ asset('storage/'.$bookTitle->file) }}" alt="Book Cover" width="50px"></td>
                    <td>
                        <a href="{{ route('book-titles.show', $bookTitle->id) }}">
                            {{ $bookTitle->title }}
                        </a>
                    </td>
                    <td>{{ $bookTitle->bookCategory->name }}</td>
                    <td>{{ $bookTitle->author->name }}</td>
                    <td>{{ $bookTitle->availableBooksCount }} / {{ $bookTitle->books->count() }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $bookTitles->links() }}
    </div>
@endsection
