@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <h1 class="display-4 mb-4">
            <i class="fas fa-columns"></i>
            Dashboard
        </h1>
        <div class="row">
            @if(auth()->user()->isAdmin() || auth()->user()->isEmployee())
                <div class="col-xs-12 col-sm-6 col-md-3 mb-4">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ route('users.index') }}">
                                <i class="fas fa-users"></i>
                                Users
                            </a>
                        </div>

                        <div class="card-body display-4 text-right">
                            {{ $usersCount }}
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-xs-12 col-sm-6 col-md-3 mb-4">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('authors.index') }}">
                            <i class="fas fa-at"></i>
                            Authors
                        </a>
                    </div>
                    <div class="card-body display-4 text-right">
                        {{ $authorsCount }}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 mb-4">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('book-titles.index') }}">
                            <i class="fas fa-book"></i>
                            Book Titles
                        </a>
                    </div>
                    <div class="card-body display-4 text-right">
                        {{ $bookTitlesCount }}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 mb-4">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('book-categories.index') }}">
                            <i class="fas fa-th-list"></i>
                            Book Categories
                        </a>
                    </div>
                    <div class="card-body display-4 text-right">
                        {{ $bookCategoriesCount }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
