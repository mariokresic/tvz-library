<?php

namespace App\Http\Controllers;

use App\Book;
use App\Http\Requests\BookRental\Create;
use App\Http\Requests\BookRental\RentBook;
use App\Http\Requests\BookRental\ReturnBook;
use App\User;
use Illuminate\Http\Request;

class BookRentalController extends Controller
{
    public function getRentBook(Create $request, Book $book) {
        $users = User::onlyUsers()->orderBy('name')->get();

        return view('book-rentals.create')->with([
            'book' => $book,
            'users' => $users
        ]);
    }

    public function rentBook(RentBook $request, Book $book) {
        $book->rentBook(User::findOrFail($request->input('user_id')));

        return redirect(route('books.show', $book->id))
            ->with('success-message', 'Successfully rented Book');
    }

    public function returnBook(ReturnBook $request, Book $book) {
        $book->returnBook();

        return redirect()->back()
            ->with('success-message', 'Successfully returned Book');
    }
}
