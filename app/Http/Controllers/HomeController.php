<?php

namespace App\Http\Controllers;

use App\Author;
use App\BookCategory;
use App\BookTitle;
use App\User;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function dash()
    {
        $usersCount = User::whereHas('role', function($q) {
            $q->where('name', 'user');
        })->count();
        $bookCategoriesCount = BookCategory::count();
        $bookTitlesCount = BookTitle::count();
        $authorsCount = Author::count();

        return view('dash')->with([
            'usersCount' => $usersCount,
            'bookCategoriesCount' => $bookCategoriesCount,
            'bookTitlesCount' => $bookTitlesCount,
            'authorsCount' => $authorsCount
        ]);
    }
}
