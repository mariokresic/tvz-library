<?php

namespace App\Http\Controllers;

use App\Book;
use App\BookTitle;
use App\Http\Requests\Book\Create;
use App\Http\Requests\Book\Delete;
use App\Http\Requests\Book\Edit;
use App\Http\Requests\Book\Store;
use App\Http\Requests\Book\Update;

class BookController extends Controller
{
    public function show(Book $book) {
        $book->load(['bookTitle', 'bookRentals.user']);

        return view('books.view')->with([
            'book' => $book
        ]);
    }

    public function edit(Edit $request, Book $book) {
        $book->load('bookTitle');

        return view('books.create-edit')->with([
            'book' => $book
        ]);
    }

    public function create(Create $request, BookTitle $bookTitle) {
        return view('books.create-edit')->with([
            'book' => new Book(),
            'bookTitle' => $bookTitle
        ]);
    }

    public function store(Store $request) {
        $book = new Book();
        $book->barcode = $request->input('barcode');
        $book->book_title_id = $request->input('book_title_id');
        $book->publication = $request->input('publication');
        $book->publication_year = $request->input('publication_year');
        $book->is_available = true;
        $book->save();

        return redirect(route('books.show', $book->id))
            ->with('success-message', 'Successfully created Book');
    }

    public function update(Update $request, Book $book) {
        $book->update($request->all());

        return redirect(route('books.show', $book->id))
            ->with('success-message', 'Successfully updated Book');
    }

    public function destroy(Delete $request, Book $book) {
        $book->delete();

        return redirect(route('book-titles.show', $book->book_title_id))->with([
            'danger-message' => 'Successfully deleted Book'
        ]);
    }
}
