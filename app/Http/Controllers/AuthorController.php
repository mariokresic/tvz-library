<?php

namespace App\Http\Controllers;

use App\Author;
use App\Http\Requests\Author\Create;
use App\Http\Requests\Author\Delete;
use App\Http\Requests\Author\Edit;
use App\Http\Requests\Author\Store;
use App\Http\Requests\Author\Update;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function index(Request $request) {
        $authorsQuery = Author::with(['bookTitles.bookCategory'])->latest('id');

        if ($request->query('birth_country'))
            $authorsQuery->where('birth_country', 'like', '%' . $request->input('birth_country') . '%');

        if ($request->query('name'))
            $authorsQuery->where('name', 'like', '%' . $request->input('name') . '%');

        $authors = $authorsQuery->paginate();

        return view('authors.browse')->with([
            'authors' => $authors
        ]);
    }

    public function show(Author $author) {
        $author->load('bookTitles.bookCategory');

        return view('authors.view')->with([
            'author' => $author
        ]);
    }

    public function edit(Edit $request, Author $author) {
        return view('authors.create-edit')->with([
            'author' => $author
        ]);
    }

    public function create(Create $request) {
        return view('authors.create-edit')->with([
            'author' => new Author()
        ]);
    }

    public function store(Store $request) {
        $author = Author::create($request->all());

        return redirect(route('authors.show', $author->id))
            ->with('success-message', 'Successfully created Author');
    }

    public function update(Update $request, Author $author) {
        $author->update($request->all());

        return redirect(route('authors.show', $author->id))
            ->with('success-message', 'Successfully updated Author');
    }

    public function destroy(Delete $request, Author $author) {
        $author->delete();

        return redirect(route('authors.index'))->with([
            'danger-message' => 'Successfully deleted Author'
        ]);
    }
}
