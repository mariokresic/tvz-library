<?php

namespace App\Http\Controllers;

use App\Role;
use App\Subscription;
use App\User;
use App\Http\Requests\User\Delete;
use App\Http\Requests\User\Store;
use App\Http\Requests\User\Update;
use App\Http\Requests\User\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(View $request) {
        $usersQuery = User::with(['bookRentals'])->onlyUsers()->latest('id');

        if ($request->query('name'))
            $usersQuery->where('name', 'like', '%' . $request->input('name') . '%');

        if ($request->query('email'))
            $usersQuery->where('email', 'like', '%' . $request->input('email') . '%');

        $users = $usersQuery->paginate();

        return view('users.browse')->with([
            'users' => $users
        ]);
    }

    public function show(View $request, User $user) {
        $user->load([
            'bookRentals.book.bookTitle'
        ]);

        return view('users.view')->with([
            'user' => $user
        ]);
    }

    public function edit(View $request, User $user) {
        $roles = Role::all();

        return view('users.create-edit')->with([
            'user' => $user,
            'roles' => $roles
        ]);
    }

    public function create(View $request) {
        $roles = Role::all();

        return view('users.create-edit')->with([
            'user' => new User(),
            'roles' => $roles
        ]);
    }

    public function store(Store $request) {
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = Hash::make('102030');
        $user->role_id = $request->input('role_id');
        $user->save();

        $subscription = new Subscription();
        $subscription->user_id = $user->id;
        $subscription->subscribed_to = now()->addMonth();
        $subscription->save();

        $user->load('subscriptions');

        return redirect(route('users.show', $user->id))
            ->with('success-message', 'Successfully created User');
    }

    public function update(Update $request, User $user) {
        $user->update($request->all());

        $user->load('subscriptions');

        return redirect(route('users.show', $user->id))
            ->with('success-message', 'Successfully updated User');
    }

    public function destroy(Delete $request, User $user) {
        $user->delete();

        return redirect(route('users.index'))->with([
            'danger-message' => 'Successfully deleted User'
        ]);
    }
}
