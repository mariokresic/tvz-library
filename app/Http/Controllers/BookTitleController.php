<?php

namespace App\Http\Controllers;

use App\Author;
use App\BookCategory;
use App\BookTitle;
use App\Http\Requests\BookTitle\Create;
use App\Http\Requests\BookTitle\Delete;
use App\Http\Requests\BookTitle\Edit;
use App\Http\Requests\BookTitle\Store;
use App\Http\Requests\BookTitle\Update;
use Illuminate\Http\Request;

class BookTitleController extends Controller
{
    public function index(Request $request) {
        $bookTitlesQuery = BookTitle::with(['author', 'bookCategory', 'books'])->latest('id');

        if ($request->query('author_id'))
            $bookTitlesQuery->where('author_id', $request->input('author_id'));

        if ($request->query('book_category_id'))
            $bookTitlesQuery->where('book_category_id', $request->input('book_category_id'));

        if ($request->query('title'))
            $bookTitlesQuery->where('title', 'like', '%' . $request->input('title') . '%');

        $bookTitles = $bookTitlesQuery->paginate();
        $bookCategories = BookCategory::all();
        $authors = Author::all();

        return view('book-titles.browse')->with([
            'bookTitles' => $bookTitles,
            'authors' => $authors,
            'bookCategories' => $bookCategories
        ]);
    }

    public function show(BookTitle $bookTitle) {
        $bookTitle->load([
            'bookCategory',
            'author',
            'books'
        ]);

        return view('book-titles.view')->with([
            'bookTitle' => $bookTitle
        ]);
    }

    public function edit(Edit $request, BookTitle $bookTitle) {
        $bookCategories = BookCategory::all();
        $authors = Author::all();

        return view('book-titles.create-edit')->with([
            'authors' => $authors,
            'bookTitle' => $bookTitle,
            'bookCategories' => $bookCategories
        ]);
    }

    public function create(Create $request) {
        $bookCategories = BookCategory::all();
        $authors = Author::all();

        return view('book-titles.create-edit')->with([
            'bookTitle' => new BookTitle(),
            'authors' => $authors,
            'bookCategories' => $bookCategories
        ]);
    }

    public function store(Store $request) {
        $bookTitle = BookTitle::create($request->all());

        $bookTitle->tryFindImage();

        return redirect(route('book-titles.show', $bookTitle->id))
            ->with('success-message', 'Successfully created Book title');
    }

    public function update(Update $request, BookTitle $bookTitle) {
        $isbnUpdated = $request->input('isbn') && $request->input('isbn') !== $bookTitle->isbn;

        $bookTitle->update($request->all());

        if ($isbnUpdated) {
            $bookTitle->tryFindImage();
        }

        return redirect(route('book-titles.show', $bookTitle->id))
            ->with('success-message', 'Successfully updated Book title');
    }

    public function destroy(Delete $request, BookTitle $bookTitle) {
        $bookTitle->delete();

        return redirect(route('book-titles.index'))->with([
            'danger-message' => 'Successfully deleted Book Title'
        ]);
    }
}
