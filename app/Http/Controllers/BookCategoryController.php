<?php

namespace App\Http\Controllers;

use App\BookCategory;
use App\Http\Requests\BookCategory\Create;
use App\Http\Requests\BookCategory\Delete;
use App\Http\Requests\BookCategory\Edit;
use App\Http\Requests\BookCategory\Store;
use App\Http\Requests\BookCategory\Update;
use Illuminate\Http\Request;

class BookCategoryController extends Controller
{
    public function index(Request $request) {
        $bookCategoriesQuery = BookCategory::latest('id');

        if ($request->query('name'))
            $bookCategoriesQuery->where('name', 'like', '%' . $request->input('name') . '%');

        $bookCategories = $bookCategoriesQuery->paginate();

        return view('book-categories.browse')->with([
            'bookCategories' => $bookCategories
        ]);
    }

    public function show(BookCategory $bookCategory) {
        return view('book-categories.view')->with([
            'bookCategory' => $bookCategory
        ]);
    }

    public function edit(Edit $request, BookCategory $bookCategory) {
        return view('book-categories.create-edit')->with([
            'bookCategory' => $bookCategory
        ]);
    }

    public function create(Create $request) {
        return view('book-categories.create-edit')->with([
            'bookCategory' => new BookCategory()
        ]);
    }

    public function store(Store $request) {
        $bookCategory = BookCategory::create($request->all());

        return redirect(route('book-categories.show', $bookCategory->id))
            ->with('success-message', 'Successfully created Book Category');
    }

    public function update(Update $request, BookCategory $bookCategory) {
        $bookCategory->update($request->all());

        return redirect(route('book-categories.show', $bookCategory->id))
            ->with('success-message', 'Successfully updated Book Category');
    }

    public function destroy(Delete $request, BookCategory $bookCategory) {
        $bookCategory->delete();

        return redirect(route('book-categories.index'))->with([
            'danger-message' => 'Successfully deleted Book Category'
        ]);
    }
}
