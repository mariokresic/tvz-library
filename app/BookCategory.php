<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookCategory extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name'
    ];

    public function bookTitles() {
        return $this->hasMany(BookTitle::class);
    }
}
