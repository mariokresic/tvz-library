<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use SoftDeletes, Notifiable;


    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id',
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];


    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    public function role() {
        return $this->belongsTo(Role::class);
    }

    public function subscriptions() {
        return $this->hasMany(Subscription::class);
    }

    public function bookRentals() {
        return $this->hasMany(BookRental::class);
    }

    public function isAdmin() {
        return $this->role->name === 'admin';
    }

    public function isEmployee() {
        return $this->role->name === 'employee';
    }

    public function scopeOnlyUsers($query) {
        return $query->whereHas('role', function($q) {
            $q->where('name', 'user');
        });
    }
}
