<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookRental extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'type',
        'user_id',
        'book_id',
        'is_returned',
        'returned_at'
    ];

    protected $casts = [
        'is_returned' => 'boolean',
        'returned_at' => 'datetime'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function book() {
        return $this->belongsTo(Book::class);
    }
}
