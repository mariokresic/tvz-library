<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Author extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'birth_year',
        'birth_country',
        'bio'
    ];

    public function bookTitles() {
        return $this->hasMany(BookTitle::class);
    }
}
