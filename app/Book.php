<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'barcode',
        'book_title_id',
        'publication',
        'publication_year',
        'is_available'
    ];

    protected $casts = [
        'is_available' => 'boolean'
    ];

    public function bookTitle() {
        return $this->belongsTo(BookTitle::class);
    }

    public function bookRentals() {
        return $this->hasMany(BookRental::class);
    }

    public function scopeOnlyAvailable($query) {
        return $query->where('is_available', true);
    }

    public function rentBook(User $user) {
        $bookRental = new BookRental();
        $bookRental->user_id = $user->id;
        $bookRental->book_id = $this->id;
        $bookRental->save();

        $this->load('bookRentals');

        return $this;
    }

    public function returnBook() {
        $bookRental = $this->bookRentals()->where('is_returned', false)->latest()->first();

        $bookRental->is_returned = true;
        $bookRental->returned_at = now();
        $bookRental->save();

        $this->is_available = true;
        return $this->save();
    }
}
