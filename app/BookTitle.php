<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class BookTitle extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'isbn',
        'file',
        'book_category_id',
        'author_id'
    ];

    public function author() {
        return $this->belongsTo(Author::class)->withTrashed();
    }

    public function bookCategory() {
        return $this->belongsTo(BookCategory::class);
    }

    public function books() {
        return $this->hasMany(Book::class);
    }

    public function getAvailableBooksCountAttribute() {
        if ($this->relationLoaded('books'))
            $this->load('books');

        return $this->books()->where('is_available', true)->count();
    }

    public function tryFindImage() {
        $url = "http://covers.openlibrary.org/b/isbn/$this->isbn-L.jpg";
        $contents = file_get_contents($url);

        $path = 'book-titles/'.substr($url, strrpos($url, '/') + 1);

        Storage::disk('public')->put($path, $contents);

        $this->file = $path;

        return $this->save();
    }
}
