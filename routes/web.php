<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dash', 'HomeController@dash')->name('dash');
    Route::resource('/book-titles', 'BookTitleController');
    Route::resource('/authors', 'AuthorController');
    Route::resource('/users', 'UserController');
    Route::resource('/book-categories', 'BookCategoryController');
    // books
    Route::post('/books', 'BookController@store')->name('books.store');
    Route::get('/books/{bookTitle}/create', 'BookController@create')->name('books.create');
    Route::put('/books/{book}', 'BookController@update')->name('books.update');
    Route::get('/books/{book}', 'BookController@show')->name('books.show');
    Route::delete('/books/{book}', 'BookController@destroy')->name('books.destroy');
    Route::get('/books/{book}/edit', 'BookController@edit')->name('books.edit');
    // rentals
    Route::post('/book-rental/return/{book}', 'BookRentalController@returnBook')->name('book-rentals.return');
    Route::get('/book-rental/rent/{book}', 'BookRentalController@getRentBook')->name('book-rentals.rent-book');
    Route::post('/book-rental/rent/{book}', 'BookRentalController@rentBook')->name('book-rentals.rent');
});



Route::get('/', 'HomeController@index')->name('home');
